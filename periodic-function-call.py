from threading import Thread, Event
import time

class sendPing(Thread):
    def __init__(self, event):
        Thread.__init__(self)
        self.stopped = event

    def run(self):
        while not self.stopped.wait(1):
            print("tick...")
            # call a function

stopFlag = Event()
thread = sendPing(stopFlag)
thread.start()
# this will stop the timer
# time.sleep(2)
# stopFlag.set()